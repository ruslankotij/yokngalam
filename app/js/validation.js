(function() {

    let me = {};
    me.isEmail = function (email) {
        let re = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/;
        return re.test(email);
    };
    me.isNumber = function (num) {
        let re = /^\d+$/;
        return re.test(num);
    };
    me.isnotEmpty = function (str) {
        return Boolean(str);
    };


    RUS.validation = me;
}());